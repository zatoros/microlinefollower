using System;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;

namespace MicroLinefollower.Drivers
{
    public class QTR
    {
        public class Sensor
        {
            private readonly AnalogInput input;

            public Sensor(
                Cpu.AnalogChannel channel)
            {
                input = new AnalogInput(
                    channel,
                    Constants.Driver.ADC_SCALE,
                    0,
                    Constants.Driver.ADC_PRECISION);
            }

            public double Read()
            {
                return input.Read();
            }
        }

        Sensor[] sensors;

        public QTR(Cpu.AnalogChannel[] sensorPins)
        {
            sensors = new Sensor[Constants.Controller.SENSOR_NUMBER];
            for (int i = 0; i < Constants.Controller.SENSOR_NUMBER; i++)
            {
                sensors[i] = new Sensor(sensorPins[i]);
            }
        }

        public double[] Read()
        {
            double[] result = new double[Constants.Controller.SENSOR_NUMBER];
            for (int i = 0; i < result.Length; i++)
                result[i] = sensors[i].Read();
            return result;
        }
    }
}
