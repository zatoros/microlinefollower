using System;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;

namespace MicroLinefollower.Drivers
{
    public class TB6612
    {
        public class Channel
        {
            public enum Direction : byte { CLOCKWISE, COUNTERCLOCKWISE };

            public readonly PWM pwm;
            public readonly OutputPort in1, in2;

            public Channel(
                Cpu.PWMChannel pwmChannel,
                Cpu.Pin in1pin,
                Cpu.Pin in2pin,
                double pwmFrequency)
            {
                in1 = new OutputPort(in1pin, false);
                in2 = new OutputPort(in2pin, false);
                pwm = new PWM(pwmChannel, pwmFrequency, Constants.Driver.MIN_DUTY_CYCLE, false);
            }

            public void SoftStop()
            {
                pwm.DutyCycle = Constants.Driver.MIN_DUTY_CYCLE;
                pwm.Stop();
                in1.Write(false);
                in2.Write(false);
            }

            public void HardStop()
            {
                pwm.DutyCycle = 0;
                pwm.Stop();
            }

            public void SetSpeed(
                double dutyCycle,
                Direction direction)
            {
                if (dutyCycle < Constants.Driver.MIN_DUTY_CYCLE)
                {
                    SoftStop();
                }
                else
                {
                    in1.Write(direction == Direction.CLOCKWISE);
                    in2.Write(direction != Direction.CLOCKWISE);
                    pwm.DutyCycle = (dutyCycle > Constants.Driver.MAX_DUTY_CYCLE)
                         ? Constants.Driver.MAX_DUTY_CYCLE : dutyCycle;
                    pwm.Start();
                }
            }
        }

        private readonly Channel channelA, channelB; 
        private readonly OutputPort stby;

        public TB6612(
            Cpu.PWMChannel pwmAchannel,
            Cpu.PWMChannel pwmBchannel,
            double pwmFreqency,
            Cpu.Pin in1Apin,
            Cpu.Pin in2Apin,
            Cpu.Pin in1Bpin,
            Cpu.Pin in2Bpin,
            Cpu.Pin stbypin)
        {
            channelA = new Channel(
                pwmAchannel,
                in1Apin,
                in2Apin,
                pwmFreqency);
            channelB = new Channel(
                pwmBchannel,
                in1Bpin,
                in2Bpin,
                pwmFreqency);
            stby = new OutputPort(stbypin, false);
        }

        public void StartDriver()
        {
            SoftStop();
            stby.Write(true);
        }

        public void Sleep()
        {
            SoftStop();
            stby.Write(false);
        }

        public void SoftStop()
        {
            channelA.SoftStop();
            channelB.SoftStop();
        }

        public void HardStop()
        {
            channelA.HardStop();
            channelB.HardStop();
        }

        public void SetSpeed(
            double dutyCycleA,
            double dutyCycleB)
        {
            SetChannelSpeed(channelA, dutyCycleA);
            SetChannelSpeed(channelB, dutyCycleB);
        }

        private void SetChannelSpeed(
            Channel channel,
            double dutyCycle)
        {
            if (dutyCycle < 0)
            {
                channel.SetSpeed(-dutyCycle, Channel.Direction.COUNTERCLOCKWISE);
            }
            else
            {
                channel.SetSpeed(dutyCycle, Channel.Direction.CLOCKWISE);
            }
        }
    }
}
