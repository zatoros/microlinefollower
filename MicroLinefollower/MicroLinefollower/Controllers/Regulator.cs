using System;
using Microsoft.SPOT;

namespace MicroLinefollower.Controllers
{
    public class Regulator
    {
        private double[] control;
        private bool initialState;

        public Regulator()
        {
            control = new double[Constants.Controller.ENGINE_NUMBER];
            initialState = true;
        }

        public double[] CountControl(double[] input)
        {
            if (System.Math.Abs(input[0] - input[1]) > Constants.Controller.HYSTERESIS || initialState)
            {
                initialState = false;
                if (input[0] > input[1])
                {
                    control[0] = Constants.Driver.MAX_DUTY_CYCLE;
                    control[1] = -Constants.Driver.MAX_DUTY_CYCLE;
                }
                else
                {
                    control[0] = -Constants.Driver.MAX_DUTY_CYCLE;
                    control[1] = Constants.Driver.MAX_DUTY_CYCLE;
                }
            }
            return control;
        }

        public void Reset()
        {
            initialState = true;
        }
    }
}
