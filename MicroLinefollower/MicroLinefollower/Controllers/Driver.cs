using System;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;

namespace MicroLinefollower.Controllers
{
    public class Driver
    {
        public class InitStruct
        {
            public Cpu.AnalogChannel[] SensorPin { set; get; }
            public Cpu.PWMChannel PwmAchannel { set; get; }
            public Cpu.PWMChannel PwmBchannel { set; get; }
            public double PwmFreqency { set; get; }
            public Cpu.Pin In1Apin { set; get; }
            public Cpu.Pin In2Apin { set; get; }
            public Cpu.Pin In1Bpin { set; get; }
            public Cpu.Pin In2Bpin { set; get; }
            public Cpu.Pin Stbypin { set; get; }
        }

        private Drivers.QTR lineSensor;
        private Drivers.TB6612 engineController;

        public Driver(InitStruct initStruct)
        {
            lineSensor = new Drivers.QTR(initStruct.SensorPin);

            engineController = new Drivers.TB6612(
                initStruct.PwmAchannel,
                initStruct.PwmBchannel,
                initStruct.PwmFreqency,
                initStruct.In1Apin,
                initStruct.In2Apin,
                initStruct.In1Bpin,
                initStruct.In2Bpin,
                initStruct.Stbypin);
            engineController.StartDriver();
        }

        public void Reset()
        {
            engineController.StartDriver();
        }

        public double[] ReadInput()
        {
            return lineSensor.Read();
        }

        public void SetOutput(double[] output)
        {
            engineController.SetSpeed(output[0], output[1]);
        }
    }
}
