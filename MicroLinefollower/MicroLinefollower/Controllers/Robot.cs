using System;
using Microsoft.SPOT;

namespace MicroLinefollower.Controllers
{
    public class ControlEventArgs : EventArgs
    {
        public double[] Input { get; private set; }
        public double[] OutPut { get; private set; }

        public ControlEventArgs(double[] input, double[] output)
        {
            Input = input;
            OutPut = output;
        }
    }

    public class Robot
    {
        private Driver driver;
        private Regulator regulator;

        private DispatcherTimer timer;

        public event EventHandler Regulate;

        public Robot(int msSystemFreq, Driver.InitStruct initStruct)
        {
            driver = new Driver(initStruct);

            regulator = new Regulator();

            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 0, 0, msSystemFreq);
            timer.Tick += new EventHandler(TimerTick);
        }

        public void StartRobot()
        {
            timer.Start();
        }

        public void StopRobot()
        {
            timer.Stop();
            driver.Reset();
            regulator.Reset();
        }

        protected void OnRegulate(ControlEventArgs args)
        {
            if (Regulate != null)
                Regulate(this, args);
        }

        private void TimerTick(object sender, EventArgs e)
        {
            double[] input = driver.ReadInput();
            double[] output = regulator.CountControl(driver.ReadInput());

            ControlEventArgs ctrlArgs = new ControlEventArgs(input, output);

            driver.SetOutput(output);
            OnRegulate(ctrlArgs);
        }
    }
}
