using System;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using Microsoft.SPOT.Input;
using Microsoft.SPOT.Presentation;
using Microsoft.SPOT.Presentation.Controls;
using Microsoft.SPOT.Presentation.Media;

using STM32F429I_Discovery.Netmf.Hardware;

namespace MicroLinefollower.Presentation
{
    class MainWindow : Window
    {
        private double[] output;
        private double[] input;
        private bool robotActive;

        InterruptPort USER_Button;

        private Font standardFont;
        private Pen standardPen;
        private Brush backgroundBrush;

        Controllers.Robot robotDriver;

        public MainWindow()
        {
            standardPen = new Pen(Color.White);
            backgroundBrush = new SolidColorBrush(Color.White);
            standardFont = Resources.GetFont(Resources.FontResources.small);

            USER_Button = new InterruptPort((Cpu.Pin)0,
                             false,
                             Port.ResistorMode.PullDown,
                             Port.InterruptMode.InterruptEdgeHigh);
            USER_Button.OnInterrupt += new NativeEventHandler(ButtonPush);

            output = new double[Constants.Controller.SENSOR_NUMBER];
            input = new double[Constants.Controller.ENGINE_NUMBER];

            Controllers.Driver.InitStruct init = new Controllers.Driver.InitStruct();
            init.SensorPin = new Microsoft.SPOT.Hardware.Cpu.AnalogChannel[]
            {
                ADC.Channel1_PA7,
                ADC.Channel3_PC3
            };
            init.PwmFreqency = Constants.Driver.PWM_FREQUENCY;
            init.PwmAchannel = PWM_Channels.PWM0_PA8;
            init.PwmBchannel = PWM_Channels.PWM1_PA9;
            init.In1Apin = (Cpu.Pin)Constants.Cpu.Pin.PE2;
            init.In2Apin = (Cpu.Pin)Constants.Cpu.Pin.PE3;
            init.In1Bpin = (Cpu.Pin)Constants.Cpu.Pin.PA5;
            init.In2Bpin = (Cpu.Pin)Constants.Cpu.Pin.PB7;
            init.Stbypin = (Cpu.Pin)Constants.Cpu.Pin.PB2;
            robotDriver = new Controllers.Robot(1000, init);
            robotDriver.Regulate += new EventHandler(TimerTick);
            robotActive = false;
        }

        public void UpdateData(
            double[] outUpdate,
            double[] inUpdate)
        {
            Array.Copy(outUpdate, output, Constants.Controller.SENSOR_NUMBER);
            Array.Copy(inUpdate, input, Constants.Controller.ENGINE_NUMBER);
            Invalidate();
        }

        private void TimerTick(object sender, EventArgs args)
        {
            Controllers.ControlEventArgs e = (Controllers.ControlEventArgs)args;
            UpdateData(e.Input, e.OutPut);
        }

        public override void OnRender(DrawingContext dc)
        {
            dc.DrawRectangle(backgroundBrush, standardPen, 0, 0,
                Constants.Window.WINDOW_WIDTH, Constants.Window.WINDOW_HEIGHT);
            if (robotActive)
            {
                dc.DrawText("Left enginee control: " + input[0].ToString("f2"),
                    standardFont, Color.Black, 15, 20);
                dc.DrawText("Right enginee control: " + input[1].ToString("f2"),
                    standardFont, Color.Black, 15, 40);
                dc.DrawText("Left sensor output: " + output[0].ToString("f2"),
                    standardFont, Color.Black, 15, 60);
                dc.DrawText("Right sensor output: " + output[1].ToString("f2"),
                    standardFont, Color.Black, 15, 80);
            }
            else
            {
                dc.DrawText("Robot inactive",
                    standardFont, Color.Black, 15, 20);
            }
        }

        private void ButtonPush(UInt32 port, UInt32 state, DateTime time)
        {
            if (!robotActive)
            {
                robotActive = true;
                robotDriver.StartRobot();
            }
            else
            {
                robotActive = false;
                robotDriver.StopRobot();
            }
        }
    }
}
