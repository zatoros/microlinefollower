using System;
using Microsoft.SPOT;

namespace MicroLinefollower.Constants
{
    public static class Driver
    {
        public const double PWM_FREQUENCY = 10000;

        public const double MIN_DUTY_CYCLE = 0.1;
        public const double MAX_DUTY_CYCLE = 0.4;

        public const byte ADC_PRECISION = 12;
        public const double ADC_SCALE = 3.3;
    }
}
