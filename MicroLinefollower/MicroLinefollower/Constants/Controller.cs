using System;
using Microsoft.SPOT;

namespace MicroLinefollower.Constants
{
    public static class Controller
    {
        public static byte SENSOR_NUMBER = 2;

        public static byte ENGINE_NUMBER = 2;

        public static double HYSTERESIS = 0.2;
    }
}
